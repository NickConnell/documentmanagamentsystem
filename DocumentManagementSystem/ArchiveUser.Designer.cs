﻿namespace DocumentManagementSystem
{
    partial class ArchiveUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblArchiveUser = new System.Windows.Forms.Label();
            this.txtArchiveUsername = new System.Windows.Forms.TextBox();
            this.lblArchiveUsername = new System.Windows.Forms.Label();
            this.btnSubmitArchiveUser = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblArchiveUser
            // 
            this.lblArchiveUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblArchiveUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArchiveUser.Location = new System.Drawing.Point(222, 12);
            this.lblArchiveUser.Name = "lblArchiveUser";
            this.lblArchiveUser.Size = new System.Drawing.Size(328, 63);
            this.lblArchiveUser.TabIndex = 13;
            this.lblArchiveUser.Text = "Archive User";
            // 
            // txtArchiveUsername
            // 
            this.txtArchiveUsername.Location = new System.Drawing.Point(326, 235);
            this.txtArchiveUsername.Name = "txtArchiveUsername";
            this.txtArchiveUsername.Size = new System.Drawing.Size(238, 20);
            this.txtArchiveUsername.TabIndex = 10;
            // 
            // lblArchiveUsername
            // 
            this.lblArchiveUsername.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblArchiveUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArchiveUsername.Location = new System.Drawing.Point(321, 204);
            this.lblArchiveUsername.Name = "lblArchiveUsername";
            this.lblArchiveUsername.Size = new System.Drawing.Size(132, 28);
            this.lblArchiveUsername.TabIndex = 8;
            this.lblArchiveUsername.Text = "Username:";
            // 
            // btnSubmitArchiveUser
            // 
            this.btnSubmitArchiveUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnSubmitArchiveUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmitArchiveUser.Location = new System.Drawing.Point(404, 296);
            this.btnSubmitArchiveUser.Name = "btnSubmitArchiveUser";
            this.btnSubmitArchiveUser.Size = new System.Drawing.Size(160, 77);
            this.btnSubmitArchiveUser.TabIndex = 7;
            this.btnSubmitArchiveUser.Text = "Submit";
            this.btnSubmitArchiveUser.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::DocumentManagementSystem.Properties.Resources.ideagenLogo;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(204, 194);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // ArchiveUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(84)))));
            this.ClientSize = new System.Drawing.Size(926, 536);
            this.Controls.Add(this.lblArchiveUser);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtArchiveUsername);
            this.Controls.Add(this.lblArchiveUsername);
            this.Controls.Add(this.btnSubmitArchiveUser);
            this.Name = "ArchiveUser";
            this.Text = "ArchiveUser";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblArchiveUser;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtArchiveUsername;
        private System.Windows.Forms.Label lblArchiveUsername;
        private System.Windows.Forms.Button btnSubmitArchiveUser;
    }
}