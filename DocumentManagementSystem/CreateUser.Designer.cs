﻿namespace DocumentManagementSystem
{
    partial class CreateUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCreateUser = new System.Windows.Forms.Label();
            this.txtCreateSurname = new System.Windows.Forms.TextBox();
            this.txtCreateForename = new System.Windows.Forms.TextBox();
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblForename = new System.Windows.Forms.Label();
            this.btnCreateUser = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtCreateUsername = new System.Windows.Forms.TextBox();
            this.lblRole = new System.Windows.Forms.Label();
            this.lblCreateUsername = new System.Windows.Forms.Label();
            this.lstbxCreateRole = new System.Windows.Forms.ListBox();
            this.txtCreatePassword = new System.Windows.Forms.TextBox();
            this.lblCreatePassword = new System.Windows.Forms.Label();
            this.txtCreateEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.btnCancelCreate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCreateUser
            // 
            this.lblCreateUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblCreateUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreateUser.Location = new System.Drawing.Point(222, 12);
            this.lblCreateUser.Name = "lblCreateUser";
            this.lblCreateUser.Size = new System.Drawing.Size(310, 60);
            this.lblCreateUser.TabIndex = 13;
            this.lblCreateUser.Text = "Create User";
            // 
            // txtCreateSurname
            // 
            this.txtCreateSurname.Location = new System.Drawing.Point(232, 211);
            this.txtCreateSurname.Name = "txtCreateSurname";
            this.txtCreateSurname.Size = new System.Drawing.Size(238, 20);
            this.txtCreateSurname.TabIndex = 11;
            // 
            // txtCreateForename
            // 
            this.txtCreateForename.Location = new System.Drawing.Point(232, 147);
            this.txtCreateForename.Name = "txtCreateForename";
            this.txtCreateForename.Size = new System.Drawing.Size(238, 20);
            this.txtCreateForename.TabIndex = 10;
            // 
            // lblSurname
            // 
            this.lblSurname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurname.Location = new System.Drawing.Point(227, 180);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(140, 28);
            this.lblSurname.TabIndex = 9;
            this.lblSurname.Text = "Surname:";
            // 
            // lblForename
            // 
            this.lblForename.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblForename.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForename.Location = new System.Drawing.Point(227, 116);
            this.lblForename.Name = "lblForename";
            this.lblForename.Size = new System.Drawing.Size(140, 28);
            this.lblForename.TabIndex = 8;
            this.lblForename.Text = "Forename:";
            // 
            // btnCreateUser
            // 
            this.btnCreateUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnCreateUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateUser.Location = new System.Drawing.Point(592, 255);
            this.btnCreateUser.Name = "btnCreateUser";
            this.btnCreateUser.Size = new System.Drawing.Size(160, 77);
            this.btnCreateUser.TabIndex = 7;
            this.btnCreateUser.Text = "Submit";
            this.btnCreateUser.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::DocumentManagementSystem.Properties.Resources.ideagenLogo;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(204, 194);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // txtCreateUsername
            // 
            this.txtCreateUsername.Location = new System.Drawing.Point(232, 276);
            this.txtCreateUsername.Name = "txtCreateUsername";
            this.txtCreateUsername.Size = new System.Drawing.Size(238, 20);
            this.txtCreateUsername.TabIndex = 16;
            // 
            // lblRole
            // 
            this.lblRole.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblRole.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRole.Location = new System.Drawing.Point(509, 116);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(140, 28);
            this.lblRole.TabIndex = 15;
            this.lblRole.Text = "Select Role:";
            // 
            // lblCreateUsername
            // 
            this.lblCreateUsername.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblCreateUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreateUsername.Location = new System.Drawing.Point(227, 245);
            this.lblCreateUsername.Name = "lblCreateUsername";
            this.lblCreateUsername.Size = new System.Drawing.Size(140, 28);
            this.lblCreateUsername.TabIndex = 14;
            this.lblCreateUsername.Text = "Username:";
            // 
            // lstbxCreateRole
            // 
            this.lstbxCreateRole.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstbxCreateRole.FormattingEnabled = true;
            this.lstbxCreateRole.ItemHeight = 18;
            this.lstbxCreateRole.Items.AddRange(new object[] {
            "Administrator",
            "Author",
            "Distributee"});
            this.lstbxCreateRole.Location = new System.Drawing.Point(514, 147);
            this.lstbxCreateRole.Name = "lstbxCreateRole";
            this.lstbxCreateRole.Size = new System.Drawing.Size(238, 58);
            this.lstbxCreateRole.TabIndex = 18;
            // 
            // txtCreatePassword
            // 
            this.txtCreatePassword.Location = new System.Drawing.Point(232, 346);
            this.txtCreatePassword.Name = "txtCreatePassword";
            this.txtCreatePassword.Size = new System.Drawing.Size(238, 20);
            this.txtCreatePassword.TabIndex = 20;
            // 
            // lblCreatePassword
            // 
            this.lblCreatePassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblCreatePassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreatePassword.Location = new System.Drawing.Point(227, 315);
            this.lblCreatePassword.Name = "lblCreatePassword";
            this.lblCreatePassword.Size = new System.Drawing.Size(140, 28);
            this.lblCreatePassword.TabIndex = 19;
            this.lblCreatePassword.Text = "Password:";
            // 
            // txtCreateEmail
            // 
            this.txtCreateEmail.Location = new System.Drawing.Point(232, 412);
            this.txtCreateEmail.Name = "txtCreateEmail";
            this.txtCreateEmail.Size = new System.Drawing.Size(238, 20);
            this.txtCreateEmail.TabIndex = 22;
            // 
            // lblEmail
            // 
            this.lblEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(227, 381);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(140, 28);
            this.lblEmail.TabIndex = 21;
            this.lblEmail.Text = "Email:";
            // 
            // btnCancelCreate
            // 
            this.btnCancelCreate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnCancelCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelCreate.Location = new System.Drawing.Point(592, 355);
            this.btnCancelCreate.Name = "btnCancelCreate";
            this.btnCancelCreate.Size = new System.Drawing.Size(160, 77);
            this.btnCancelCreate.TabIndex = 23;
            this.btnCancelCreate.Text = "Cancel";
            this.btnCancelCreate.UseVisualStyleBackColor = false;
            // 
            // CreateUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(84)))));
            this.ClientSize = new System.Drawing.Size(926, 536);
            this.Controls.Add(this.btnCancelCreate);
            this.Controls.Add(this.txtCreateEmail);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.txtCreatePassword);
            this.Controls.Add(this.lblCreatePassword);
            this.Controls.Add(this.lstbxCreateRole);
            this.Controls.Add(this.txtCreateUsername);
            this.Controls.Add(this.lblRole);
            this.Controls.Add(this.lblCreateUsername);
            this.Controls.Add(this.lblCreateUser);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtCreateSurname);
            this.Controls.Add(this.txtCreateForename);
            this.Controls.Add(this.lblSurname);
            this.Controls.Add(this.lblForename);
            this.Controls.Add(this.btnCreateUser);
            this.Name = "CreateUser";
            this.Text = "CreateUser";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCreateUser;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtCreateSurname;
        private System.Windows.Forms.TextBox txtCreateForename;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblForename;
        private System.Windows.Forms.Button btnCreateUser;
        private System.Windows.Forms.TextBox txtCreateUsername;
        private System.Windows.Forms.Label lblRole;
        private System.Windows.Forms.Label lblCreateUsername;
        private System.Windows.Forms.ListBox lstbxCreateRole;
        private System.Windows.Forms.TextBox txtCreatePassword;
        private System.Windows.Forms.Label lblCreatePassword;
        private System.Windows.Forms.TextBox txtCreateEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Button btnCancelCreate;
    }
}