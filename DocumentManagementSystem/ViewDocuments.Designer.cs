﻿namespace DocumentManagementSystem
{
    partial class ViewDocuments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewDocuments));
            this.lblDocumentManagement = new System.Windows.Forms.Label();
            this.lblYourDocuments = new System.Windows.Forms.Label();
            this.btnBackToUser = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnDoc1 = new System.Windows.Forms.Button();
            this.btnDoc2 = new System.Windows.Forms.Button();
            this.btnDoc3 = new System.Windows.Forms.Button();
            this.btnDoc7 = new System.Windows.Forms.Button();
            this.btnDoc6 = new System.Windows.Forms.Button();
            this.btnDoc5 = new System.Windows.Forms.Button();
            this.btnDoc4 = new System.Windows.Forms.Button();
            this.pctbxAddDocument = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctbxAddDocument)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDocumentManagement
            // 
            this.lblDocumentManagement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblDocumentManagement.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocumentManagement.Location = new System.Drawing.Point(222, 12);
            this.lblDocumentManagement.Name = "lblDocumentManagement";
            this.lblDocumentManagement.Size = new System.Drawing.Size(575, 63);
            this.lblDocumentManagement.TabIndex = 18;
            this.lblDocumentManagement.Text = "Document Management";
            // 
            // lblYourDocuments
            // 
            this.lblYourDocuments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblYourDocuments.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblYourDocuments.Location = new System.Drawing.Point(227, 95);
            this.lblYourDocuments.Name = "lblYourDocuments";
            this.lblYourDocuments.Size = new System.Drawing.Size(371, 28);
            this.lblYourDocuments.TabIndex = 15;
            this.lblYourDocuments.Text = "Documents you can view and edit";
            this.lblYourDocuments.Click += new System.EventHandler(this.lblYourDocuments_Click);
            // 
            // btnBackToUser
            // 
            this.btnBackToUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnBackToUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackToUser.Location = new System.Drawing.Point(789, 458);
            this.btnBackToUser.Name = "btnBackToUser";
            this.btnBackToUser.Size = new System.Drawing.Size(114, 57);
            this.btnBackToUser.TabIndex = 14;
            this.btnBackToUser.Text = "Back";
            this.btnBackToUser.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::DocumentManagementSystem.Properties.Resources.ideagenLogo;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(204, 194);
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // btnDoc1
            // 
            this.btnDoc1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDoc1.BackgroundImage")));
            this.btnDoc1.Location = new System.Drawing.Point(368, 142);
            this.btnDoc1.Name = "btnDoc1";
            this.btnDoc1.Size = new System.Drawing.Size(112, 153);
            this.btnDoc1.TabIndex = 20;
            this.btnDoc1.UseVisualStyleBackColor = true;
            // 
            // btnDoc2
            // 
            this.btnDoc2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDoc2.BackgroundImage")));
            this.btnDoc2.Location = new System.Drawing.Point(506, 142);
            this.btnDoc2.Name = "btnDoc2";
            this.btnDoc2.Size = new System.Drawing.Size(112, 153);
            this.btnDoc2.TabIndex = 21;
            this.btnDoc2.UseVisualStyleBackColor = true;
            // 
            // btnDoc3
            // 
            this.btnDoc3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDoc3.BackgroundImage")));
            this.btnDoc3.Location = new System.Drawing.Point(645, 142);
            this.btnDoc3.Name = "btnDoc3";
            this.btnDoc3.Size = new System.Drawing.Size(112, 153);
            this.btnDoc3.TabIndex = 22;
            this.btnDoc3.UseVisualStyleBackColor = true;
            // 
            // btnDoc7
            // 
            this.btnDoc7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDoc7.BackgroundImage")));
            this.btnDoc7.Location = new System.Drawing.Point(645, 301);
            this.btnDoc7.Name = "btnDoc7";
            this.btnDoc7.Size = new System.Drawing.Size(112, 153);
            this.btnDoc7.TabIndex = 26;
            this.btnDoc7.UseVisualStyleBackColor = true;
            // 
            // btnDoc6
            // 
            this.btnDoc6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDoc6.BackgroundImage")));
            this.btnDoc6.Location = new System.Drawing.Point(506, 301);
            this.btnDoc6.Name = "btnDoc6";
            this.btnDoc6.Size = new System.Drawing.Size(112, 153);
            this.btnDoc6.TabIndex = 25;
            this.btnDoc6.UseVisualStyleBackColor = true;
            // 
            // btnDoc5
            // 
            this.btnDoc5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDoc5.BackgroundImage")));
            this.btnDoc5.Location = new System.Drawing.Point(368, 301);
            this.btnDoc5.Name = "btnDoc5";
            this.btnDoc5.Size = new System.Drawing.Size(112, 153);
            this.btnDoc5.TabIndex = 24;
            this.btnDoc5.UseVisualStyleBackColor = true;
            // 
            // btnDoc4
            // 
            this.btnDoc4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDoc4.BackgroundImage")));
            this.btnDoc4.Location = new System.Drawing.Point(232, 301);
            this.btnDoc4.Name = "btnDoc4";
            this.btnDoc4.Size = new System.Drawing.Size(112, 153);
            this.btnDoc4.TabIndex = 23;
            this.btnDoc4.UseVisualStyleBackColor = true;
            // 
            // pctbxAddDocument
            // 
            this.pctbxAddDocument.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pctbxAddDocument.BackgroundImage")));
            this.pctbxAddDocument.Location = new System.Drawing.Point(232, 142);
            this.pctbxAddDocument.Name = "pctbxAddDocument";
            this.pctbxAddDocument.Size = new System.Drawing.Size(112, 153);
            this.pctbxAddDocument.TabIndex = 27;
            this.pctbxAddDocument.TabStop = false;
            // 
            // ViewDocuments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(84)))));
            this.ClientSize = new System.Drawing.Size(926, 536);
            this.Controls.Add(this.pctbxAddDocument);
            this.Controls.Add(this.btnDoc7);
            this.Controls.Add(this.btnDoc6);
            this.Controls.Add(this.btnDoc5);
            this.Controls.Add(this.btnDoc4);
            this.Controls.Add(this.btnDoc3);
            this.Controls.Add(this.btnDoc2);
            this.Controls.Add(this.btnDoc1);
            this.Controls.Add(this.lblDocumentManagement);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblYourDocuments);
            this.Controls.Add(this.btnBackToUser);
            this.Name = "ViewDocuments";
            this.Text = "ViewDocuments";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctbxAddDocument)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblDocumentManagement;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblYourDocuments;
        private System.Windows.Forms.Button btnBackToUser;
        private System.Windows.Forms.Button btnDoc1;
        private System.Windows.Forms.Button btnDoc2;
        private System.Windows.Forms.Button btnDoc3;
        private System.Windows.Forms.Button btnDoc7;
        private System.Windows.Forms.Button btnDoc6;
        private System.Windows.Forms.Button btnDoc5;
        private System.Windows.Forms.Button btnDoc4;
        private System.Windows.Forms.PictureBox pctbxAddDocument;
    }
}